import React, {useState} from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  FlatList,
  ScrollView,
  ImageBackground,
} from 'react-native';
import Header from './src/components/Header';
import MenuBar from './src/components/MenuBar';
import Jadwal from './src/components/Jadwal';
import imgPerson from './src/images/doctor.jpg'

const App = () => {
  const [pencarian, setPencarian] = useState('');
  const [kategori, setKategori] = useState([
    {namaKategori: 'Artikel'},
    {namaKategori: 'Konsultasi'},
    {namaKategori: 'Obat'},
    {namaKategori: 'Cari Dokter'},
    {namaKategori: 'Klinik Terdekat'},
    {namaKategori: 'Rumah Sakit'},
  ]);

  const [artikel, setArtikel] = useState([
    {judul: 'Resep Rahasia Agar Tetap Sehat',
    deskripsi: 'Inilah resep rahasia agar anda tetap sehat di rumah',
    image: imgPerson
  }
  ])

  return (
    <View style={{flex: 1, backgroundColor: '#f4f4f4'}}>
      <ScrollView showsVerticalScrollIndicator={false } style={{flex: 1, marginHorizontal: 20, marginTop: 10}}>
        <Header pencarian={pencarian} setPencarian={setPencarian} />
        <Jadwal />
        <View style={{flexDirection: 'row', marginTop: 30}}>
          <Text style={{color: '#0082f7', fontWeight: 'bold'}}>Kategori</Text>
          <TouchableOpacity
            style={{justifyContent: 'center', alignItems: 'flex-end', flex: 1}}>
            <Text style={{color: '#FDB436', fontWeight: 'bold'}}>
              Lihat semua
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <FlatList
            data={kategori}
            horizontal
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  paddingVertical: 10,
                  marginRight: 10,
                  paddingHorizontal: 10,
                  backgroundColor: 'white',
                  borderRadius: 25,
                  elevation: 3,
                  marginBottom: 10,
                  marginTop: 10,
                }}>
                <Text> {item.namaKategori} </Text>
              </TouchableOpacity>
            )}></FlatList>
        </View>
         <View>
          <FlatList
            data={artikel}
            horizontal
            style={{ marginBottom: 20 }}
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 10,
                  backgroundColor: 'white',
                  borderRadius: 10,
                  elevation: 3,
                  marginBottom: 10,
                  marginTop: 10,
                  paddingBottom: 20
                }}>
                <View style={{height: 200, marginBottom: 10}}>
                  <ImageBackground source={item.image} 
        style={{flex:1, borderTopRightRadius: 10, borderTopLeftRadius: 10}} 
          imageStyle={{  borderTopRightRadius: 10, borderTopLeftRadius: 10}}
        />
                </View>
                <View>
<Image source={item.image} 
        style={{marginTop: 5, marginLeft: 10, width:40, height: 40, borderRadius: 20, borderColor: 'white', borderWidth: 2}} />
                <Text style={{marginTop: 5, marginHorizontal: 10, fontWeight: 'bold', fontSize: 18 }}> {item.judul} </Text>
               <Text  style={{ marginHorizontal: 10}}> {item.deskripsi} </Text>
                </View>
        
              </TouchableOpacity>
            )}></FlatList>
        </View>
      </ScrollView>
      <MenuBar />
    </View>
  );
};

export default App;
