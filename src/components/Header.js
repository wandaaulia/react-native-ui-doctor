import React from 'react';
import {TouchableOpacity, View, Text, StatusBar, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const Header = (props) => {
  return (
    <View>
       <StatusBar barStyle="dark-content" backgroundColor="#f4f4f4" />
        <Text style={{color: '#212121'}}> Hello </Text>
        <Text style={{fontSize: 22, fontWeight: 'bold', color: '#212121'}}>
          Youu
        </Text>
        <View style={{flexDirection: 'row'}}>
          <TextInput
            value={props.pencarian}
            onChangeText={text => props.setPencarian(text)}
            placeholder="Cari informasi tentang kesehatan / dokter"
            style={{
              backgroundColor: 'white',
              elevation: 3,
              marginTop: 20,
              paddingLeft: 10,
              borderRadius: 5,
              flex: 1,
            }}
          />
          <TouchableOpacity
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#0002f7',
              marginTop: 20,
              paddingHorizontal: 10,
              borderRadius: 5,
              marginLeft: 10,
            }}>
            <Icon name="search" size={23} color="#ffffff" />
          </TouchableOpacity>
              </View>
    </View>
  )
}

export default Header
