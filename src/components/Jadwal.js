import React, {useState} from 'react';
import {TouchableOpacity, View, Text, StatusBar, TextInput, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import imgDoctor from '.././images/doctor.jpg';

const Jadwal = () => {
  return (
   <View>
         <View style={{flexDirection: 'row', marginTop: 20 }}>
        <Text style={{ color: '#0082f7', fontWeight: 'bold' }}>
          Jadwal Pemeriksaan
        </Text>
        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'flex-end', flex: 1 }}>
          <Text style={{ color: '#FDB436', fontWeight: 'bold' }}> 
            Lihat semua
          </Text>
        </TouchableOpacity>
      </View>
      <View style={{ marginTop: 10, backgroundColor: 'white', borderRadius: 10, elevation: 10 }}> 
      <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#018bf7', '#00baf7']}
      style={{ borderRadius: 10 }}>
      <TouchableOpacity style={{ padding: 20, borderRadius: 10 }}> 
        <View style={{ flexDirection: 'row' }}> 
        <Image source={imgDoctor} 
        style={{width:50, height: 50, borderRadius: 25, borderColor: 'white', borderWidth: 2}} />
        <View style={{ flex:1, marginLeft: 10, justifyContent: 'center' }}> 
        <Text style={{ color:'white', fontWeight: 'bold' }}> Dr.Adi </Text>
        <Text style={{ color: '#f4f4f4' }}> Dokter Umum </Text>
        </View>
        </View>   
        <View style={{ marginTop:20 }}> 
        <View style={{ flexDirection: 'row' }}>
        <Icon name="search" size={23} color="#ffffff" />
        <View style={{justifyContent:'center', alignItems:'center' }}>
        <Text style={{ color:'white', marginLeft: 10}}> 6 April 2022 </Text>
        </View>
        </View>
        <View style={{ flexDirection: 'row' }}>
        <Icon name="compass" size={23} color="#ffffff" />
        <View style={{justifyContent:'center', alignItems:'center' }}>
        <Text style={{ color:'white', marginLeft: 10}}> Klinik Medika </Text>
        </View>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
        <Icon name="chevron-forward-circle" size={35} color="#ffffff" />
        </View>
        </View>
      </TouchableOpacity>
    </LinearGradient>
    </View>
   </View>
  )
}

export default Jadwal
